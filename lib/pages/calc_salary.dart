import 'package:flutter/material.dart';

class CalcSalary extends StatefulWidget {
  const CalcSalary({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CalcSalary();
  }
}

class _CalcSalary extends State<CalcSalary> {
  double? deviceWidth, deviceHeight;

  String? salary, percetage;

  String result = "";

  final GlobalKey<FormState> inputForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    deviceHeight = MediaQuery.of(context).size.height;
    deviceWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.purpleAccent,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: deviceWidth! * 0.05,
          ),
          child: salarySection(),
        ),
      ),
    );
  }

  Widget salarySection() {
    return Form(
      key: inputForm,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          title(),
          userInputs(),
          textOutput(),
          confirmButton(),
        ],
      ),
    );
  }

  Widget title() {
    return const Text(
      "Enter current salary and raise percetage",
      style: TextStyle(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget userInputs() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        inputSalary(),
        inputPercent(),
      ],
    );
  }

  Widget inputSalary() {
    return TextFormField(
      style: const TextStyle(
        color: Colors.white,
      ),
      decoration: const InputDecoration(
        hintText: "Salary Yearly",
        hintStyle: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.5),
        ),
        errorStyle: TextStyle(color: Colors.white),
      ),
      onSaved: (value) {
        setState(() {
          salary = value;
        });
      },
      validator: (value) {
        if (value!.isEmpty) {
          return "Salary Input can't be empty";
        } else if (!(value!.contains(RegExp(r'[0-9]')))) {
          return "Salary Input must be a number";
        }
      },
    );
  }

  Widget inputPercent() {
    return TextFormField(
      style: const TextStyle(
        color: Colors.white,
      ),
      decoration: const InputDecoration(
        hintText: "Percentage %",
        hintStyle: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 0.5),
        ),
        errorStyle: TextStyle(color: Colors.white),
      ),
      onSaved: (value) {
        setState(() {
          percetage = value;
        });
      },
      validator: (value) {
        if (value!.isEmpty) {
          return "Salary Input can't be empty";
        } else if (!(value!.contains(RegExp(r'[0-9]')))) {
          return "Salary Input must be a number";
        }
      },
    );
  }

  Widget confirmButton() {
    return MaterialButton(
      color: Colors.blueAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      onPressed: () {
        calculateSalary();
      },
      child: const Text("Calculate"),
    );
  }

  Widget textOutput() {
    return SizedBox(
      height: deviceHeight! * 0.10,
      child: Text(
        result,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 20,
        ),
      ),
    );
  }

  void calculateSalary() {
    if (inputForm.currentState!.validate()) {
      inputForm.currentState!.save();
     
      double s = double.parse(salary!);
      double p = double.parse(percetage!) * .01;

      double r = (s * p) + s;

      setState(() {
        result = "New Salary: ${r.toString().padRight(2, '0')}";
      });
    }
  }
}
